import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { GlobalProvider } from "./context/GlobalState";
import Header from "./components/Header";
import Watched from "./components/Watched";
import WatchList from "./components/WatchList";
import Add from "./components/Add";

const App = () => {
  return (
    <GlobalProvider>
      <Router>
        <Header />

        <div className="container pt-5">
          <Switch>
            <Route exact component={WatchList} path="/" />
            <Route exact component={Watched} path="/watched" />
            <Route exact component={Add} path="/add" />
          </Switch>
        </div>
      </Router>
    </GlobalProvider>
  );
};

export default App;
