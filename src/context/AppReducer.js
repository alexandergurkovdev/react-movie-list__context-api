const AppReducer = (state, { type, payload }) => {
  switch (type) {
    case "ADD_MOVIE_TO_WATCHLIST":
      return {
        ...state,
        watchlist: [payload, ...state.watchlist],
      };

    default:
      return state;
  }
};

export default AppReducer;
