import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalState";

const MovieCard = ({ item }) => {
  const {
    title,
    poster_path,
    overview,
    release_date,
    vote_average,
    vote_count,
  } = item;

  const {
    addMovieToWatchList,
    state: { watchlist },
  } = useContext(GlobalContext);

  let storedMovie = watchlist.find((o) => o.id === item.id);

  return (
    <div className="card d-flex flex-row align-items-center mb-3">
      {poster_path && (
        <img
          src={`https://image.tmdb.org/t/p/w200${poster_path}`}
          className="card-img-top"
          alt={title}
          style={{ maxWidth: 150 }}
        />
      )}

      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text mb-2">{overview}</p>
        <ul className="p-0" style={{ listStyle: "none" }}>
          <li>
            <i className="bi bi-star-fill me-2" />
            {vote_average} ({vote_count})
          </li>
          <li>
            <i className="bi bi-calendar-event me-2" />
            {release_date.split("-").reverse().join(".")}
          </li>
        </ul>
        <button
          className="btn btn-primary"
          onClick={() => addMovieToWatchList(item)}
          disabled={storedMovie}
        >
          {storedMovie ? "Added" : "+ Add to watchlist"}
        </button>
      </div>
    </div>
  );
};

export default MovieCard;
