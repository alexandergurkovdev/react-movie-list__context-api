import React from "react";
import { Link, NavLink } from "react-router-dom";

const Header = () => {
  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <div className="container-fluid">
          <Link className="navbar-brand" to="/">
            Logo
          </Link>

          <ul className="navbar-nav align-items-center mb-0 ms-auto">
            <li className="nav-item">
              <NavLink
                exact
                to="/"
                className="nav-link"
                activeClassName="active"
              >
                Watch list
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/watched"
                className="nav-link"
                activeClassName="active"
              >
                Watched
              </NavLink>
            </li>
            <li className="nav-item ms-2">
              <NavLink exact to="/add" className="btn btn-success btn-sm">
                + Add
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  );
};

export default Header;
