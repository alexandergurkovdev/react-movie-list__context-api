import React, { useState, useEffect, Fragment } from "react";
import MovieCard from "./MovieCard";

const Add = () => {
  const [term, setTerm] = useState("");
  const [data, setData] = useState([]);

  useEffect(() => {
    async function fetchMovies() {
      await fetch(
        `https://api.themoviedb.org/3/search/movie?api_key=${process.env.REACT_APP_TMDB_KEY}&language=en-US&page=1&include_adult=false&query=${term}`
      )
        .then((res) => res.json())
        .then((data) => {
          if (!data.errors) {
            setData(data.results);
          } else {
            setData([]);
          }
        })
        .catch((e) => console.log(e));
    }

    if (term) fetchMovies();
  }, [term]);

  return (
    <Fragment>
      <div className="input-group mb-3 w-100">
        <span className="input-group-text" id="search">
          <i className="bi bi-search" />
        </span>
        <input
          type="text"
          placeholder="search for the movie"
          className="form-control"
          aria-describedby="search"
          value={term}
          onChange={(e) => setTerm(e.target.value)}
        />
      </div>

      {data.map((item) => {
        return <MovieCard key={item.id} item={item} />;
      })}
    </Fragment>
  );
};

export default Add;
